
Url: https://dev-wanderapp.pantheonsite.io/create_journey.json

Request Params:
{
    "title":"Bar Crawl",
    "description":"This is my first test journey",
    "journey_category":["cafe","restaurant","food","point_of_interest","store","establishment"],
    "journey_contact_email":"Test@mail.com",
    "journey_start_time":"1577435312",
    "journey_end_time":"1577406512",
    "journey_location":["Marlton Square 300 Route 73 South, Marlton","This is another location","This is alternate location"]
    "journey_phone_number":"123456789",
    "journey_region":"East Zone",
    "tagline":"This is my new tag line",
    "guide_id":"71"
}

Response:
{
    "data": {
        "title": "Bar Crawl"
    },
    "status": true,
    "message": "Journey created successfully."
}